import { useState } from "react";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from "@mui/system/Container";
import Fab from '@mui/material/Fab';
import AddIcon from "@mui/icons-material/Add";

// import './App.css';
import TodoList from './TodoList';
import CreateModal from "./modals/CreateModal";
import {TodoProvider, defaultState} from "./TodoContext";

function App() {
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const openCreateModal = () => setCreateModalOpen(true)
  const closeCreateModal = () => setCreateModalOpen(false)

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography
            variant="h6"
            noWrap
            component="div"
          >
            TodoList
          </Typography>
        </Toolbar>
      </AppBar>

      <TodoProvider value={defaultState}>
        <Container className="content">
          <TodoList />

          <Fab color="primary" aria-label="add" className="add-item-fab" onClick={openCreateModal}>
            <AddIcon />
          </Fab>

          <CreateModal open={createModalOpen} handleClose={closeCreateModal} />
        </Container>
      </TodoProvider>
    </>
  );
}

export default App;
