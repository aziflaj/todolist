import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

const Modal = ({open, handleClose}) => (
  <Dialog open={open} onClose={handleClose}>
    <DialogTitle>Delete</DialogTitle>
    <DialogContent>
      <DialogContentText>
        A je i sigurt qe do e fshish?
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={handleClose}>Cancel</Button>
      <Button onClick={handleClose}>Delete</Button>
    </DialogActions>
  </Dialog>
);

export default Modal
