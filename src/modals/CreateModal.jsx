import { useState } from "react";
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import TextField from '@mui/material/TextField';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

import { TodoConsumer } from "../TodoContext";

export default function CreateModal({open, handleClose}) {
  const [data, setData] = useState("")

  return (
    <TodoConsumer>
      {
        ({addItem}) => {
          return (
          <Dialog open={open} onClose={handleClose}>
            <DialogTitle>Create new item</DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="todo-item"
                type="text"
                fullWidth
                value={data}
                onChange={(e) => setData(e.target.value)}
                variant="standard"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button onClick={() => { addItem(data); setData(""); handleClose() }}>Save</Button>
            </DialogActions>
          </Dialog>
          )
        }
      }
    </TodoConsumer>
  );
}
