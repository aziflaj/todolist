import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import TextField from '@mui/material/TextField';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

const Modal = ({open, handleClose, data}) => (
  <Dialog open={open} onClose={handleClose}>
    <DialogTitle>Edit item</DialogTitle>
    <DialogContent>
      <TextField
        autoFocus
        margin="dense"
        id="todo-item"
        type="text"
        fullWidth
        value={data}
        variant="standard"
      />
    </DialogContent>
    <DialogActions>
      <Button onClick={handleClose}>Cancel</Button>
      <Button onClick={handleClose}>Save</Button>
    </DialogActions>
  </Dialog>
);

export default Modal
