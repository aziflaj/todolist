import { TodoConsumer } from "./TodoContext";
import ListItem from "./ListItem";

export default function TodoList() {
  return (
    <TodoConsumer>
      {
        ({list}) => list.map((item, index) => (
          <ListItem key={index} data={item} />
        ))
      }
    </TodoConsumer>
  )
}
