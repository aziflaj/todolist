import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import EditIcon from '@mui/icons-material/Edit';
import {Grid} from "@mui/material";

import DeleteModal from "./modals/DeleteModal";
import EditModal from "./modals/EditModal"

export default function ListItem({data}) {
  const [deleteModalOpen, setDeleteModalOpen] = useState(false)
  const openDeleteModal = () => setDeleteModalOpen(true)
  const closeDeleteModal = () => setDeleteModalOpen(false)

  const [editModalOpen, setEditModalOpen] = useState(false)
  const openEditModal = () => setEditModalOpen(true)
  const closeEditModal = () => setEditModalOpen(false)

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={9} className="list-item">
          {data}
        </Grid>

        <Grid item xs={3}>
          <IconButton aria-label="edit" color="primary" onClick={openEditModal}>
            <EditIcon />
          </IconButton>
          <IconButton aria-label="delete" color="error" onClick={openDeleteModal}>
            <DeleteForeverIcon />
          </IconButton>
        </Grid>
      </Grid>

      <DeleteModal open={deleteModalOpen} handleClose={closeDeleteModal}/>
      <EditModal open={editModalOpen} handleClose={closeEditModal} data={data}/>
    </>
  )
}
