import { createContext } from "react";

const TodoContext = createContext([]);

const defaultState = {
  list: [],
  addItem: (item) => defaultState.list.push(item),
  editItem: (index, item) => defaultState.list[index] = item,
  deleteItem: (index) => defaultState.list.splice(index, 1)
}

const TodoProvider = TodoContext.Provider;
const TodoConsumer = TodoContext.Consumer;

export { defaultState, TodoProvider, TodoConsumer };
export default TodoContext;
